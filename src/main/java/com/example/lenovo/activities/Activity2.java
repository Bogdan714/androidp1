package com.example.lenovo.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Activity2 extends AppCompatActivity {
    @BindView(R.id.lifeTime)
    TextView myText;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);
        ButterKnife.bind(this);

        User user;

        if (getIntent() != null
                && getIntent().getParcelableExtra(MainActivity.KEY_USER) != null) {
            user = getIntent().getParcelableExtra(MainActivity.KEY_USER);
            long[] times = new long[3];
            times[0] = ((System.currentTimeMillis()/3600000/24) - user.getLife())/365;//лет
            times[1] = (System.currentTimeMillis()/3600000/24) - user.getLife();//дней
            times[2] = (System.currentTimeMillis()/1000) - (user.getLife()*24*3600);//секунд


            myText.setText(String.format("Привет %s, твой знак - %s. Тебе сейчас %s лет, или %s дней, или %s секунд\n.", user.getName(),user.getZodiacSign(), times[0]+"", times[1]+"", times[2]+""));
        }
    }
}

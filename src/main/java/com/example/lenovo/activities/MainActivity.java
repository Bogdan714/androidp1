package com.example.lenovo.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

import java.sql.Date;
import java.util.GregorianCalendar;


import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    public static final String KEY_USER = "KU";

    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.date)
    EditText date;

    User user;
    static final int[] monthLen = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31, 28};
    static final String[] signs = {"Овен", "Телец", "Близнецы", "Рак", "Лев", "Дева",
            "Весы", "Скорпион", "Стрелец", "Козерог", "Водолей", "Рыбы"};
    static final int[][] daysOfSigns = {{21, 21, 21, 22, 23, 24, 24, 24, 23, 22, 21, 21},
            {20, 20, 21, 22, 23, 23, 23, 22, 21, 20, 20, 20}};
    static final int[][] monthsOfSigns = {{3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 1, 2},
            {4, 5, 6, 7, 8, 9, 10, 11, 12, 1, 2, 3}};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        if (savedInstanceState != null) {
            user = savedInstanceState.getParcelable(KEY_USER);
        }
    }

    @OnClick(R.id.button1)
    public void onClick() {
        String dateString = date.getText().toString();
        int days = 0;
        int month = 0;
        int year = 0;
        try {
            days = Integer.parseInt(dateString.substring(0, dateString.indexOf(".")));
            month = Integer.parseInt(dateString.substring(dateString.indexOf(".") + 1, dateString.lastIndexOf(".")));
            year = Integer.parseInt(dateString.substring(dateString.lastIndexOf(".") + 1, dateString.length()));

        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        }
        if (days > 0 && days < monthLen[month]
                && month > 0 && month < 13
                && year > 0 && year <= (new java.util.Date().getYear() + 1900)) {

            long daysOfThatTime = 0;
            for (int i = 0; i < month - 1; i++) {
                if (i == 2) {
                    daysOfThatTime += monthLen[12];
                } else {
                    daysOfThatTime += monthLen[i];
                }
            }
            daysOfThatTime += days + ((year - 1970) * 365);
            String sign = "";
            for (int i = 0; i < 12; i++) {
                if (days >= daysOfSigns[0][i] && month == monthsOfSigns[0][i]
                        || days <= daysOfSigns[1][i] && month == monthsOfSigns[1][i]) {
                    sign = signs[i];
                    break;
                }
            }
            //Long fromThatTime = (System.currentTimeMillis()) - (daysOfThatTime * 24l * 60l * 60l);

            user = new User(name.getText().toString(), daysOfThatTime, sign);
            Intent intent = new Intent(this, Activity2.class);
            intent.putExtra(KEY_USER, user);
            startActivity(intent);
        } else {
            System.out.println("incorrect input");
        }

    }
}

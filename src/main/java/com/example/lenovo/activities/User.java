package com.example.lenovo.activities;

import android.os.Parcel;
import android.os.Parcelable;

public class User implements Parcelable {
    private String name;
    private Long lifeInDays;
    private String zodiacSign;

    public Lib lib;

    public User(String name, Long life, String zodiacSign) {
        this.name = name;
        this.lifeInDays = life;
        this.zodiacSign = zodiacSign;
    }

    protected User(Parcel in) {
        name = in.readString();
        lifeInDays = Long.parseLong(in.readString());
        zodiacSign = in.readString();
        lib = in.readParcelable(Lib.class.getClassLoader());
    }


    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public String getName() {
        return name;
    }

    public Long getLife() {
        return lifeInDays;
    }

    public String getZodiacSign() {
        return zodiacSign;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(lifeInDays.toString());
        dest.writeString(zodiacSign);
        dest.writeParcelable(lib, flags);
    }
}
